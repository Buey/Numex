Numex
======

Get written numbers (and money quantities) from numbers.  In Brazilian Portuguese.

Usage
======

Basic numbers:

```
>>> import numex
>>> numex.numeroPorExtenso(2398764)
>>> 'dois milhões, trezentos e noventa e oito mil, setecentos e sessenta e quatro'
```

Money quantities (brazilian Real):

```
>>> import dindin
>>> dindin.granaPorExtenso('R$897123')
>>> 'oitocentos e noventa e sete mil, cento e vinte e três reais'
```

Also with cents:

```
>>> import dindin
>>> dindin.granaPorExtenso('R$23,27')
>>> 'vinte e três reais e vinte e sete centavos'
```

License
=========

MIT
