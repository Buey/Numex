if __name__ == "dindin":
    import numex
else:
    from . import numex


def grana(quant):
    """Parse the money quantity."""
    quant = str(quant)
    result = [None, None]
    hasCents = False

    # Checking input
    if quant == "":
        raise ValueError
    for character in quant:
        if character not in "R$.,0123456789":
            raise ValueError
    if not quant.startswith("R$"):
        raise ValueError

    if quant[-3] == "," and quant.count(",") == 1:
        hasCents = True

    quant = quant.lstrip("R$")

    if hasCents:
        quant = quant.split(",", maxsplit=1)
        quant[0] = quant[0].replace(".", "")
        for i in range(2):
            numQuant = int(quant[i])
            result[i] = numQuant if numQuant >= 1 else None
    else:
        result[0] = int(quant.replace(".", ""))

    return result


def granaPorExtenso(quant):
    """Return a written money quantity, from a number representing a money quantity."""
    quant = grana(quant)
    moeda = (("real", "reais"), ("centavo", "centavos"))
    bufunfa = []

    for num, moe in zip(quant, moeda):
        if num is None:
            bufunfa.append(num)
            bufunfa.append(None)
        elif num > 1:
            bufunfa.append(numex.numeroPorExtenso(num))
            bufunfa.append(moe[1])
        elif num == 1:
            bufunfa.append(numex.numeroPorExtenso(num))
            bufunfa.append(moe[0])

    if not None in bufunfa:
        bufunfa.insert(2, "e")
    else:
        bufunfa = [elem for elem in bufunfa if elem is not None]

    try:
        if not quant[0] % 1000000 != 0:
            bufunfa.insert(1, "de")
    except TypeError:
        pass

    return " ".join(bufunfa)
