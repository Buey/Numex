import pytest

from numex import dindin, numex


class TestPorExtenso:

    def testNumberOutOfBounds(self):
        with pytest.raises(ValueError):
            numex.porExtenso(-1)
            numex.porExtenso(1000)

    def testOneDigit(self):
        assert numex.porExtenso(0) == "zero"
        assert numex.porExtenso(4) == "quatro"

    def testTwoDigits(self):
        cases = {
            10: "dez",
            19: "dezenove",
            20: "vinte",
            23: "vinte e três",
            99: "noventa e nove",
        }
        for k, v in cases.items():
            assert numex.porExtenso(k) == v

    def testThreeDigits(self):
        cases = {
            100: "cem",
            101: "cento e um",
            110: "cento e dez",
            119: "cento e dezenove",
            123: "cento e vinte e três",
            200: "duzentos",
            223: "duzentos e vinte e três",
            999: "novecentos e noventa e nove",
        }
        for k, v in cases.items():
            assert numex.porExtenso(k) == v

    def testNotInts(self):
        with pytest.raises(ValueError):
            numex.porExtenso(1.5)
            numex.porExtenso(1e2)
            numex.porExtenso(1+1j)
            numex.porExtenso(0xFFF)

    def testWrongTypes(self):
        with pytest.raises(TypeError):
            cases = ["foo", "999", [], "0b10", "0xFFF"]
            for c in cases:
                numex.porExtenso(c)
            numex.porExtenso()


class TestNumeroPorExtenso:
    def testNumberOutOfBounds(self):
        with pytest.raises(ValueError):
            numex.numeroPorExtenso(-1)
            numex.numeroPorExtenso(999999999999999999999999999999999999)

    def testThousands(self):
        cases = {
            0: "zero",
            123: "cento e vinte e três",
            1000: "mil",
            # Python's thousands separator is ok.
            1_000: "mil",
            1001: "mil e um",
            1900: "mil e novecentos",
            1975: "mil, novecentos e setenta e cinco",
            10000: "dez mil",
            10001: "dez mil e um",
            10900: "dez mil e novecentos",
            37521: "trinta e sete mil, quinhentos e vinte e um",
        }
        for k, v in cases.items():
            assert numex.numeroPorExtenso(k) == v

    def testMillionsAndAbove(self):
        cases = {
            1000000: "um milhão",
            1234000: "um milhão, duzentos e trinta e quatro mil",
            1234100: "um milhão, duzentos e trinta e quatro mil e cem",
            12345678: "doze milhões, trezentos e quarenta e cinco "
            "mil, seiscentos e setenta e oito",
            999999999999999: "novecentos e noventa e nove trilhões, "
            "novecentos e noventa e nove bilhões, novecentos e "
            "noventa e nove milhões, novecentos e "
            "noventa e nove mil, novecentos e noventa e nove",
        }
        for k, v in cases.items():
            assert numex.numeroPorExtenso(k) == v


class TestDindin:
    def testGranaWrongInput(self):
        with pytest.raises(ValueError):
            cases = [
                1,
                "",
                "A",
                "RS1000",
                "US$1000",
                "1000",
                "R$0,999",
                "R$10,999",
            ]
            for c in cases:
                dindin.grana(c)

    def testGranaNoCents(self):
        cases = {
            "R$1": [1, None],
            "R$1000": [1000, None],
            "R$1.000": [1000, None],
            "R$1.234.567": [1234567, None],
        }
        for k, v in cases.items():
            dindin.grana(k) == v

    def testGranaWithCents(self):
        cases = {
            "R$0,01": [None, 1],
            "R$0,99": [None, 99],
            "R$1,00": [1, None],
            "R$1,52": [1, 52],
            "R$1000,00": [1000, None],
            "R$1123,47": [1123, 47],
            "R$1.000,00": [1000, None],
            "R$1.123,47": [1123, 47],
        }
        for k, v in cases.items():
            dindin.grana(k) == v

    def testGranaPorExtensoNoSymbol(self):
        with pytest.raises(ValueError):
            dindin.granaPorExtenso(1)
            dindin.granaPorExtenso("R$0,999")
            dindin.granaPorExtenso("R$10,999")

    def testGranaPorExtensoNoCents(self):
        cases = {
            "R$1": "um real",
            "R$1000": "mil reais",
            "R$1.000": "mil reais",
            "R$1.000.000": "um milhão de reais",
            "R$123456789": "cento e vinte e três milhões, quatrocentos "
            "e cinqüenta e seis mil, setecentos e oitenta e nove reais",
        }
        for k, v in cases.items():
            assert dindin.granaPorExtenso(k) == v

    def testGranaPorExtensoWithCents(self):
        cases = {
            "R$0,01": "um centavo",
            "R$0,99": "noventa e nove centavos",
            "R$1,00": "um real",
            "R$1,52": "um real e cinqüenta e dois centavos",
            "R$1000,00": "mil reais",
            "R$1123,47": "mil, cento e vinte e três reais e quarenta e sete centavos",
            "R$1.000,00": "mil reais",
            "R$1.123,47": "mil, cento e vinte e três reais e quarenta e sete centavos",
            "R$1.000.000,36": "um milhão de reais e trinta e seis centavos",
            "R$1.234.000,22": "um milhão, duzentos e trinta e quatro mil "
            "reais e vinte e dois centavos",
        }
        for k, v in cases.items():
            assert dindin.granaPorExtenso(k) == v
