import itertools


def numeroPorExtenso(num):
    """Return a written number (between 0 and 999999999999999)."""

    if num < 0 or num > 999_999_999_999_999:
        raise ValueError

    mults = {
        0: (None, None),
        1: ("mil", "mil"),
        2: ("milhão", "milhões"),
        3: ("bilhão", "bilhões"),
        4: ("trilhão", "trilhões"),
    }
    agrupado = agrupadoPorMilhar(num)
    agrupado.reverse()
    numeros, multiplos, conectores = [], [], []
    acimaDeMil = len(agrupado) > 1

    if not acimaDeMil:
        return porExtenso(num)
    else:
        for i, n in enumerate(agrupado):

            # Define the connections between groups (comma or 'e')
            entreUmENoventaENove = 1 <= agrupado[0] and agrupado[0] <= 99
            multiploDeCem = not ((agrupado[0] == 0) or (agrupado[0] % 100 != 0))

            if len(agrupado) < 3:
                if (i == len(agrupado) - 1) or (agrupado[0] == 0):
                    conectores.append(None)
                elif i == 0 and (entreUmENoventaENove or multiploDeCem):
                    conectores.append(" e ")
                else:
                    conectores.append(", ")
            else:
                if (i == len(agrupado) - 1) or n == 0:
                    conectores.append(None)
                elif i == 0 and (entreUmENoventaENove or multiploDeCem):
                    conectores.append(" e ")
                else:
                    conectores.append(", ")

            # Define the number to be inserted in the final version.
            zeroNaUnidade = n == 0
            umNoMilhar = i == 1 and n == 1
            if zeroNaUnidade or umNoMilhar:
                numeros.append(None)
            elif i == 0:
                numeros.append(porExtenso(n))
            else:
                numeros.append(porExtenso(n) + " ")

            # Define the multiples to be inserted in the final version.
            ePlural = 1 if n > 1 else 0
            if i > 0 and n == 0:
                multiplos.append(None)
            else:
                multiplos.append(mults[i][ePlural])

    for y in (conectores, numeros, multiplos):
        y.reverse()

    extenso = [s for s in zip(conectores, numeros, multiplos)]
    extenso = [s for s in itertools.chain(*extenso) if s is not None]
    extenso = "".join(extenso)

    return extenso


def porExtenso(num):
    """Return a written number (between 0 and 999)."""

    if num < 0 or num > 999:
        raise ValueError

    u = {
        0: "zero",
        1: "um",
        2: "dois",
        3: "três",
        4: "quatro",
        5: "cinco",
        6: "seis",
        7: "sete",
        8: "oito",
        9: "nove",
    }
    d = {
        0: "",
        1: (
            "dez",
            "onze",
            "doze",
            "treze",
            "quatorze",
            "quinze",
            "dezesseis",
            "dezessete",
            "dezoito",
            "dezenove",
        ),
        2: "vinte",
        3: "trinta",
        4: "quarenta",
        5: "cinqüenta",
        6: "sessenta",
        7: "setenta",
        8: "oitenta",
        9: "noventa",
    }
    c = {
        0: "",
        1: ("cem", "cento"),
        2: "duzentos",
        3: "trezentos",
        4: "quatrocentos",
        5: "quinhentos",
        6: "seiscentos",
        7: "setecentos",
        8: "oitocentos",
        9: "novecentos",
    }

    cStr, dStr, uStr = "", "", ""  # 100ths, 10ths, units
    bloc = [int(x) for x in str(num).rjust(3, "0")]
    centEdezFechada = bloc[2] == 0 and (bloc[0] != 0 or bloc[1] != 0)
    umaDezenaEAlgo = bloc[1] == 1

    if bloc[0] != 1:  # Pick the right 100th
        cStr = c[bloc[0]]
    else:
        if bloc[1] == 0 and bloc[2] == 0:
            cStr = c[bloc[0]][0]
        else:
            cStr = c[bloc[0]][1]

    if bloc[1] != 1:  # Pick the right 10th
        dStr = d[bloc[1]]
    else:
        if bloc[2] == 0:
            dStr = d[bloc[1]][0]
        else:
            dStr = d[bloc[1]][bloc[2]]

    if centEdezFechada or umaDezenaEAlgo:  # Pick the right unit
        uStr = ""
    else:
        uStr = u[bloc[2]]

    numStr = [s for s in (cStr, dStr, uStr) if s != ""]  # Everything together

    if len(numStr) == 1:
        return numStr[0]
    elif len(numStr) > 1:
        numStr = " e ".join(numStr)
        return numStr


def agrupadoPorMilhar(num):
    """Return the digits in groups of three."""
    agrupado = [n for n in grouper(3, reversed(str(num)), padvalue="")]
    agrupado = [int("".join(reversed(g))) for g in agrupado]
    agrupado.reverse()
    return agrupado


# Got this from Itertools recipes
# https://docs.python.org/3/library/itertools.html
def grouper(n, iterable, padvalue=None):
    """grouper(3, 'abcdefg', 'x') -->
    ('a','b','c'), ('d','e','f'), ('g','x','x')"""
    return itertools.zip_longest(*[iter(iterable)] * n, fillvalue=padvalue)
